from typing import Final


def validateInput(hours: int, subscription: str):
    if (hours > 8):
        print("Erreur. Vous ne pouvez pas louer un vélo plus de 8 heures")
        exit(1)
    elif hours < 1:
        print("Erreur. Vous ne pouvez pas louer un vélo moins d'une heure")
        exit(1)

    sub = subscription.lower()
    if sub != "oui" and sub != "non":
        print("Erreur. Veuillez répondre par 'oui' ou 'non'")
        exit(1)


def textToBool(text: str) -> bool:
    if text.lower() == "oui":
        return True
    elif text.lower() == "non":
        return False
    else:
        raise ValueError("Veuillez entrer 'oui' ou 'non'")


def calculatePrice(hours: int, sub: bool) -> int:
    count = 0
    if (hours > 2):
        count = 3 * 2 + 5 * (hours - 2)
    else:
        count = 3 * hours

    if not sub:
        count = count + 2

    return count


if __name__ == "__main__":
    print("Location de vélos!")
    hours = int(input("Combien d'heures souhaitez-vous louer ?"))
    subscription_text = input("Avez-vous un abonnement ?")
    validateInput(hours, subscription_text)

    try:
        subscription = textToBool(subscription_text)
        price = calculatePrice(hours, subscription)
        print(
            f"Le montant prévisionnel pour {hours} heures de location est de {price} euros."
        )
    except ValueError as err:
        print(f"Une erreur s'est produite: {err}")
