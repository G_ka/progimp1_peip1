"""
Écrire une procédure dessinePolygone qui dessine un polygone régulier. Les paramètres 
sont le nombre de cotés, la longueur d'un coté, le point en bas à gauche du polygone, la couleur du polygone. 
Cette procédure reprend tout simplement le programme avec boucle du TD précédent.
"""

import turtle as turtle


def dessinePolygone(sidesCount: int,
                    lenght: float,
                    origX: int = 0,
                    origY: int = 0,
                    color: str = "black") -> None:
    turtle.penup()
    turtle.goto(origX, origY)
    turtle.pendown()

    turtle.color(color)

    angle = 360 / sidesCount
    for _ in range(sidesCount):
        turtle.forward(lenght)
        turtle.left(angle)


if __name__ == '__main__':
    dessinePolygone(5, 100, 50, 50, "red")