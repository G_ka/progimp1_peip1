"""
En utilisant la procédure dessineCarre écrire
un programme qui dessine une diagonale de 
carrés avec un facteur d'accroissement. 
La largeur initiale du carré, le nombre 
de carrés, le facteur d'accroissement 
sont lus au clavier. 
"""

import turtle


def dessinePolygone(sidesCount: int,
                    lenght: float,
                    origX: int = 0,
                    origY: int = 0,
                    color: str = "black") -> None:
    turtle.penup()
    turtle.goto(origX, origY)
    turtle.pendown()

    turtle.color(color)

    angle = 360 / sidesCount
    for _ in range(sidesCount):
        turtle.forward(lenght)
        turtle.left(angle)


# dessine un carré de largeur "cote", de couleur "couleur" dont le point
# en bas à gauche est en (x,y)
def dessineCarre(cote, x, y, couleur):
    dessinePolygone(4, cote, x, y, couleur)


def drawSquares(firstLenght: float, squareNum: int, grow: float) -> None:
    origX = -200
    origY = -200
    lenght = firstLenght
    for _ in range(squareNum):
        dessineCarre(lenght, origX, origY, "black")
        origX += lenght
        origY += lenght
        lenght *= grow


if __name__ == "__main__":
    drawSquares(20, 10, 1.25)