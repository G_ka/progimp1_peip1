"""
Utiliser la fonction affine dans un programme de conversion entre degré Celsius et degré 
Fahrenheit sachant que : T(°C) = (T(°F) - 32)/1.8 et T(°F) = T(°C)×1.8 + 32 . 
Le programme demande la température et demande l'unité (Celsius ou Fahrenheit) et il affiche la valeur convertie.
"""

from fonctions import *


def toFahrenheit(val: int) -> float:
    return affine(1.8, 32, val)


def toCelsius(val: int) -> float:
    return affine(1 / 1.8, 0, affine(1, -32, val))


if __name__ == '__main__':
    unit = input("Cible : Celsius (C) ou Fahrenheit (F)?")
    val = int(input("Valeur ?"))

    if unit == "C":
        print(toCelsius(val))
    elif unit == "F":
        print(toFahrenheit(val))
    else:
        print("Unité non existante")
