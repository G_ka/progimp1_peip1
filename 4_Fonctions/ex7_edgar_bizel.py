"""
Utilisez cette procédure dans un programme qui affiche un sapin stylisé c'est à dire 
un carré marron surmonté d'un triangle vert. Le côté du  triangle mesure 2 fois celui du carré
"""

import turtle as turtle


def dessinePolygone(sidesCount: int,
                    lenght: float,
                    origX: int = 0,
                    origY: int = 0,
                    color: str = "black") -> None:
    turtle.penup()
    turtle.goto(origX, origY)
    turtle.pendown()

    turtle.color(color)

    angle = 360 / sidesCount
    for _ in range(sidesCount):
        turtle.forward(lenght)
        turtle.left(angle)


def drawPineTree(lenght: float, origX: int = 0, origY: int = 0):
    dessinePolygone(4, lenght, origX, origY, "brown")
    triangleX = int(origX - lenght / 2)
    triangleY = int(origY + lenght)
    dessinePolygone(3, lenght * 2, triangleX, triangleY, "green")


if __name__ == "__main__":
    drawPineTree(50)