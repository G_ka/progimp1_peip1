"""
Déco and Co. Écrire une fonction surfaceMur(hauteur,largeur,longueur) qui prend 
les caractéristiques d'une pièce et renvoie la surface à peindre en m2. 
Utilisez cette fonction dans un programme qui lit une hauteur, une largeur,  
un longueur (des float) et affiche la surface des murs 
(on suppose qu'il n'y a ni porte ni fenêtre). 
Par exemple, surfaceMur(2.5,5,8.3) doit renvoyer 66.5.
"""


def surfaceMur(hauteur: float, largeur: float, longueur: float) -> float:
    return 2 * (largeur * hauteur + longueur * hauteur)


if __name__ == '__main__':
    height = float(input("Hauteur ?"))
    width = float(input("Largeur ?"))
    lenght = float(input("Longueur ?"))
    print(surfaceMur(height, width, lenght))
