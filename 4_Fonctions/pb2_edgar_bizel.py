"""
On suppose que le tarif des AirBNB est à la nuit, et identique quelle que 
soit la période. Écrire un programme qui lit le prix de la nuitée, qui 
lit la date d'entrée et la date de sortie et qui affiche le prix du séjour. 
Les dates sont lues au format jj/mm/aaaa.
"""
from enum import Enum


class DateErrorType(Enum):
    NoIssue = 0
    Day = 1
    Month = 2
    Year = 3

    def __str__(self):
        switch = {
            0: "Pas de problème",
            1: "Jour incorrect",
            2: "Mois incorrect",
            3: "Année incorrecte",
        }
        return switch.get(self.value)


class Date():
    #Note: The date may be invalid after construction, but not the format
    def __init__(self, date: str):
        if not Date.isValidFormat(date):
            raise ValueError("Date must be greater than 10 characters")

        self.day = int(date[0:2])
        self.month = int(date[3:5])
        self.year = int(date[6:10])

        self.minDay = 1
        self.maxDay = 0
        self.__calculateMaxDay()

        self.minMonth = 1
        self.maxMonth = 12

        self.minYear = 1600
        self.maxYear = 2018

    @staticmethod
    def isValidFormat(date: str):
        return len(date) == 10 and date[2] == date[5] == "/"

    def validityState(self) -> DateErrorType:
        # Une année est comprise entre 1600 et 2018
        if self.year < self.minYear or self.year > self.maxYear:
            return DateErrorType(DateErrorType.Day)

        # Un mois est compris entre 1 et 12
        if self.month < self.minMonth or self.month > self.maxMonth:
            return DateErrorType(DateErrorType.Month)

        if self.day < self.minDay or self.day > self.maxDay:
            return DateErrorType(DateErrorType.Year)

        return DateErrorType(DateErrorType.NoIssue)

    def isLeapYear(self) -> bool:
        return (self.year % 4 == 0
                and self.year % 100 != 0) or (self.year % 400 == 0)

    def advanceDay(self):
        self.day += 1
        if self.day > self.maxDay:
            self.day = self.minDay
            self.month += 1
            if self.month > self.maxMonth:
                self.month = self.minMonth
                self.year += 1
        self.__calculateMaxDay()

    def __str__(self):
        #Utilisation des modificateurs pour ajouter des 0
        return f"{self.day:02d}/{self.month:02d}/{self.year:02d}"

    def __eq__(self, other):
        return self.day == other.day and self.month == other.month and self.year == other.year

    #Assigne le jour maximal autorisé selon le mois et l'année
    def __calculateMaxDay(self) -> None:
        #Un jour est compris entre 1 et 31 pour les mois 1, 3, 5, 7, 8, 10 et 12.
        #Il est compris entre 1 et 30 pour les mois 4,6,9 et 11
        #Si le mois est 2, alors le jour est compris entre 1 et 29
        # les années bissextiles et entre 1 et 28 les années non bissextiles
        if self.month in [1, 3, 5, 7, 8, 10, 12]:
            self.maxDay = 31
        elif self.month != 2:
            self.maxDay = 30
        else:
            if self.isLeapYear():
                self.maxDay = 29
            else:
                self.maxDay = 28


def difference(dateOne: Date, dateTwo: Date) -> int:
    #Limite arbitraire pour éviter les boucles infinies
    #Dans l'idéal, il faudrait vérifier que dateOne < dateTwo
    maxCounter = 1000000
    counter = 0
    while (dateOne != dateTwo and counter < maxCounter):
        dateOne.advanceDay()
        counter += 1

    return counter


def promptDate(text: str) -> Date:
    dateStr = input(f"{text}\nFormat attendu : JJ/MM/AAAA\n")
    if not Date.isValidFormat(dateStr):
        print("Format invalide")
        exit(1)

    date = Date(dateStr)
    if date.validityState() != DateErrorType.NoIssue:
        print(f"Date invalide: {date.validityState()}")
        exit(1)

    return date


if __name__ == '__main__':
    dateOne = promptDate("Date de début ?")
    dateTwo = promptDate("Date de fin ?")
    prix = float(input("Prix de la nuit ? (En euros)"))
    total = difference(dateOne, dateTwo) * prix
    print(f"Coût total: {total}€")