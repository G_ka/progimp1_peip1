"""
Reprenez votre code de la table de multiplication générale 
(i.e. où l'on peut dire quelle table on veut réviser et jusqu'à quel rang) 
et transformez-le en une procédure. Écrivez un code qui fait une boucle où 
l'on demande à l'utilisateur quelle table il veut réviser et jusqu'à quel 
rang, puis qui demande s'il veut continuer. 
"""


# Fonction récupérée de l'exercice 2 du cours 3
def printTable(num: int, max: int) -> None:
    print(f"Révision de la table de {num} jusqu'à {max}")
    for i in range(1, max + 1):
        print(f"{i} fois {num} : {i * num}")


def reviewTables():
    print("Révision des tables de multiplication")
    while True:
        table = int(input("Quelle table voulez-vous réviser ?"))
        max = int(input("Jusqu'à quel rang ?"))

        printTable(table, max)

        if input("Encore ? (o/n)") != "o":
            break

        print("Fin de la révision")


if __name__ == '__main__':
    reviewTables()