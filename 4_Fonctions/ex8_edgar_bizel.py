"""
Une fonction ou procédure peut appeler une autre fonction ou procédure
Définissez de cette façon les fonctions dessineTriangle et dessineCercle. 
"""

import turtle


def dessinePolygone(sidesCount: int,
                    lenght: float,
                    origX: int = 0,
                    origY: int = 0,
                    color: str = "black") -> None:
    turtle.penup()
    turtle.goto(origX, origY)
    turtle.pendown()

    turtle.color(color)

    angle = 360 / sidesCount
    for _ in range(sidesCount):
        turtle.forward(lenght)
        turtle.left(angle)


# dessine un carré de largeur "cote", de couleur "couleur" dont le point
# en bas à gauche est en (x,y)
def dessineCarre(cote, x, y, couleur):
    dessinePolygone(4, cote, x, y, couleur)


def dessineTriangle(lenght: float,
                    x: int = 0,
                    y: int = 0,
                    color: str = "black"):
    dessinePolygone(3, lenght, x, y, color)


def dessineCercle(lenght: float, x: int = 0, y: int = 0, color: str = "black"):
    dessinePolygone(20, lenght, x, y, color)


if __name__ == "__main__":
    dessineCercle(20, -100, -100)
    dessineTriangle(100, 100, 100)
