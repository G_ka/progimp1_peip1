"""
Utiliser cette fonction dans un programme qui permet de traiter toutes 
les pièces d'un appartement (onsuppose que la hauteur est la même dans 
toutes les pièces). Le programme lit la hauteur de l'appartement, le 
nombre de pièces et pour chaque pièce (i.e. à chaque itération), le 
programme lit le nom, la largeur, la longueur de la pièce et affiche 
la surface à peindre dans cette pièce.  A la fin, le programme affiche 
la surface totale des murs de l'appartement.
"""


def surfaceMur(hauteur: float, largeur: float, longueur: float) -> float:
    return 2 * (largeur * hauteur + longueur * hauteur)


def promptRoom(height: float):
    width = float(input("Largeur ?"))
    lenght = float(input("Longueur ?"))
    return surfaceMur(height, width, lenght)


if __name__ == '__main__':
    numRoom = int(input("Nombre de pièces ?"))
    height = float(input("Hauteur des pièces ?"))
    total = 0
    for i in range(numRoom):
        name = input("Nom de la pièce ?")
        surface = promptRoom(height)
        total += surface
        print(f"{surface}m² à peindre dans {name}")
    print(f"Total: {total}m²")
