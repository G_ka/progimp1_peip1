"""
Modifiez le programme de la table de multiplication par 7 pour qu'il soit plus général. Pour cela, utilisez la fonction input pour demander à l'utilisateur quelle table de multiplication il veut réviser, et jusqu'à quelle valeur il veut aller. Le résultat de l'exécution de votre programme doit être le suivant :

Quelle table voulez-vous réviser ? 3
Jusqu'à quelle valeur ? 12
Révision de la table de 3 jusqu'à 12
1 fois 3  : 3
2 fois 3  : 6
3 fois 3  : 9
4 fois 3  : 12
5 fois 3  : 15
6 fois 3  : 18
7 fois 3  : 21
8 fois 3  : 24
9 fois 3  : 27
10 fois 3  : 30
11 fois 3  : 33
12 fois 3  : 36
fin de la révision
"""


def printTable(num: int, max: int) -> None:
    print(f"Révision de la table de {table} jusqu'à {max}")
    for i in range(1, max + 1):
        print(f"{i} fois {num} : {i * num}")


if __name__ == '__main__':
    table = int(input("Quelle tablez voulez-vous réviser ?"))
    max = int(input("Jusqu'à quelle valeur ?"))

    printTable(table, max)