"""
Écrire un programme qui lit un entier n et calcule la factorielle de cet entier c'est à dire la multiplication de 
tous les entiers compris entre 1 et n. Tester votre programme pour des petites valeurs comme 4 ou 6 pour vérifier qu'il 
st correct. Tester votre programme pour 1000 et 10000. Que se passe-t-il ? 
"""

# Plus le nombre est grand, plus le temps de traitement est élevé
# En Python, il n'y a pas d'overflow


def factorielle(num: int) -> int:
    assert num > 1, "Le nombre doit être strictement positif"

    sum = 1
    while num > 0:
        sum *= num
        num = num - 1

    return sum


if __name__ == '__main__':
    num = int(input("Nombre ?"))
    print(factorielle(num))