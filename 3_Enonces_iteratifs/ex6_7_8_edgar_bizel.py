"""
6: Écrire un programme qui dessine un carré dont la longueur des côtés est fournie par l'utilisateur.
7: Écrire un programme qui dessine un triangle équilatéral dont la longueur des côtés est fournie par l'utilisateur.
8: Écrire un programme qui dessine un hexagone dont la longueur des côtés est fournie par l'utilisateur.
"""

import turtle as turtle


def square(size: int) -> None:
    for i in range(4):
        turtle.forward(size)
        turtle.right(90)


def triangle(size: int) -> None:
    for i in range(3):
        turtle.forward(size)
        turtle.right(120)


def hexagone(size: int):
    for i in range(6):
        turtle.forward(size)
        turtle.right(60)


if __name__ == '__main__':
    size = int(input("Longueur ?"))
    shape = int(
        input("Quelle forme ? Carré (1), Triangle (2) ou Hexagone (3) ?"))
    if shape == 1:
        square(size)
    elif shape == 2:
        triangle(size)
    elif shape == 3:
        hexagone(size)
    else:
        print("Cette forme n'existe pas")
        exit(1)

    turtle.Screen().exitonclick()