"""
Reprenez le programme de la table de multiplication par 7 et enlever l'instruction i = i+1. Que se passe-t-il ? 
"""

# table de multiplication par 7
print("révision de la table de 7")
i = 1
while i <= 10:
    print(i, "fois 7 :", i * 7)
    #i = i + 1  # Si on enlève cette instruction, une boucle infinie va se produire
print("fin de la révision")