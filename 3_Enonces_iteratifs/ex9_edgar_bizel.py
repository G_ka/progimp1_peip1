"""
Écrire un programme qui lit un nombre de carrés, la largeur de leurs côtés et qui dessine ces 
carrés en diagonale à partir du point (-350,-300) en bas à gauche.
"""

import turtle as turtle


def init():
    turtle.up()
    turtle.goto(-350, -300)
    turtle.down()


def printSquares(count: int, size: int) -> None:
    for _ in range(count):
        for _ in range(6):
            turtle.forward(size)
            turtle.left(90)
        turtle.setheading(0)


if __name__ == '__main__':
    init()
    squareNumber = int(input("Nombre de carrés ?"))
    squareSize = int(input("Taille des carrés ?"))

    printSquares(squareNumber, squareSize)
    turtle.Screen().exitonclick()