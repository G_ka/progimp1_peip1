"""
Écrire un programme qui lit un nombre entier n (en base 10), qui lit une base b et qui affiche le nombre n converti en base b. 
"""

def convert(num: int, base: int) -> int:
    result = ""
    while (num // base or num % base):
        result = str(num % base) + result
        num = num // base

    return int(result)


def test():
    assert (convert(8, 2) == 1000)
    assert (convert(148, 2) == 10010100)
    assert (convert(6, 3) == 20)
    assert (convert(145, 3) == 12101)
    print("Tests réussis")


if __name__ == '__main__':
    test()

    num = int(input("Nombre ?"))
    base = int(input(" Base ?"))
    print(convert(num, base))
