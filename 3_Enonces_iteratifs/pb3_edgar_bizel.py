"""
On vous demande de dessinez des polygones inscrits les uns dans les autres.
Votre programme demande à l'utilisateur :
nbPoly : le nombre de polygones,
nbCote : le nombre de cotés du polygone (>3),
l : la longueur des cotés du 1er polygone,
f : un facteur qui donne la position relative du polygone i+1 par rapport au point de départ du polygone i 
(par exemple, le polygone i+1 commence à 50% du polygone i, c'est à dire à sa moitié).
"""

import turtle as turtle
import math as math


#Voir le schéma
def prepareNext(lenght: float, gap: float, GBK: float) -> float:
    GB = lenght * gap
    turtle.forward(GB)

    GK = GB * math.sin(math.radians(GBK))
    BK = math.sqrt(GB**2 - GK**2)

    FK = lenght - GB + BK
    #Methode Julien (marche pour les triangles)
    #FB = lenght - GB
    #FK = FB + GB * math.cos(math.radians(GBK))

    FG = math.sqrt(FK**2 + GK**2)
    GFB = math.degrees(math.asin(GK / FG))

    turtle.left(GFB)

    return FG


def drawShapes(num: int, sides: int, lenght: float, gap: float):
    angle = 360 / sides
    for _ in range(num):
        for _ in range(sides):
            turtle.forward(lenght)
            turtle.left(angle)
        lenght = prepareNext(lenght, gap, angle)


if __name__ == "__main__":
    turtle.speed(0)
    drawShapes(num=500, sides=20, lenght=50.0, gap=0.1)
    turtle.Screen().exitonclick()
