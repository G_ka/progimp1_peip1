"""
Écrire un programme qui dessine un escalier qui descend en partant du coin en haut 
à gauche de la fenêtre. Le nombre de marches, la hauteur et la longueur des marches 
sont fournies par l'utilisateur. Attention : le dessin de l'escalier ne doit pas 
sortir de la fenêtre. Écrivez "départ" en haut de l'escalier. Écrivez "arrivée" 
en bas de l'escalier si vous avez pu afficher toutes les marches, ou "tronqué" 
si vous n'avez pu afficher qu'une partie de l'escalier.
"""

import turtle as turtle
from typing import Final


def init():
    topLeftCornerWidth = -turtle.window_width() / 2 + 10
    topLeftCornerHeight = turtle.window_height() / 2 - 20
    turtle.speed(0)
    turtle.penup()
    turtle.goto(topLeftCornerWidth, topLeftCornerHeight)
    turtle.pendown()
    turtle.write("Départ")


def drawStair(height: int, width: int) -> None:
    turtle.setheading(0)  #Est
    turtle.forward(width)
    turtle.setheading(270)  #South
    turtle.forward(height)


def canWrite(width: int, height: int) -> bool:
    highWidthLim: Final = (turtle.window_width() / 2) - 10
    lowHeightLim: Final = -(turtle.window_height() / 2) + 10

    x, y = turtle.pos()
    return (x + width) < highWidthLim and (y - height) > lowHeightLim


def drawStairs(num: int, height: int, width: int) -> None:
    for _ in range(num):
        if (not canWrite(width, height)):
            turtle.write("Tronqué")
            return

        drawStair(height, width)

    turtle.write("Arrivé")


if __name__ == "__main__":
    num = int(input("Nombre de marches ?"))
    height = int(input("Hauteur des marches ?"))
    width = int(input("Largeur des marches ?"))

    init()
    drawStairs(num, height, width)

    turtle.Screen().exitonclick()