"""
Écrire un programme qui tire aléatoirement un nombre entre 0 et 100 et demande au 
joueur de donner un nombre. Si le nombre fourni est trop petit ou trop grand, on 
affiche un message correspondant. Si le nombre est trouvé, on affiche le nombre de 
tentatives effectuées. On suppose que l'on a au plus 7 tentatives, au delà, c'est perdu !

Exemple d'exécution :

Vous devez trouver un nombre compris entre 1 et 100
Vous avez au plus 7 tentatives
le nombre ? 45
trop grand
le nombre ? 12
trop grand
le nombre ? 3
trop petit
le nombre ? 6
vous avez gagné en 4 tentatives 
"""

from typing import Final
from random import randint

# Constants
minNum: Final = 1
maxNum: Final = 100

maxTry: Final = 7


def playGuess() -> None:
    answer: Final = randint(minNum, maxNum)
    tryNum = 0
    guess = maxNum + 1
    while answer != guess and tryNum < maxTry:
        tryNum += 1
        guess = int(input("Le nombre ?"))
        if (guess > answer):
            print("Trop grand")
        else:
            print("Trop petit")

    if guess == answer:
        print(f"Vous avez gagné en {tryNum} tentatives")
    else:
        print(
            f"Dommage, vous ferez mieux la prochaine fois. La réponse était {answer}"
        )


if __name__ == '__main__':
    print(f"Vous devez trouver un nombre compris entre {minNum} et {maxNum}")
    print(f"Vous avez au plus {maxTry} tentatives")

    playGuess()
