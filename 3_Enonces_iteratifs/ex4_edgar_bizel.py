"""
Nombres pairs. Écrire un programme qui lit un nombre minimum mini et un nombre maximum maxi 
et qui affiche tous les nombres pairs compris entre mini et maxi.
"""


def printEven(min: int, max: int):
    for i in range(min, max + 1):
        if (i % 2 == 0):
            print(i)


if __name__ == '__main__':
    min = int(input("Minimum ?"))
    max = int(input("Maximum ?"))
    printEven(min, max)
