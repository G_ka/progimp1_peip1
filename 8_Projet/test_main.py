from main import quillesVersImprimable, ligneValide, jouerMilieu, jouerCote, jouer, ordiJoue
import pytest


def test_quillesVersImprimable():
    assert quillesVersImprimable([[0, 6]], 7) == "|||||||"
    assert quillesVersImprimable([[0, 6]], 10) == "|||||||..."
    assert quillesVersImprimable([[0, 1], [4, 6], [8, 8]], 10) == "||..|||.|."


def test_ligneValide():
    assert not ligneValide(-1, [[]])
    assert not ligneValide(1, [])
    assert ligneValide(1, [[], []])


@pytest.mark.parametrize('compteur_repet', range(50))
# On lance le test plusieurs fois car les résultats sont aléatoires
def test_jouerMilieu(compteur_repet):
    entree = [[[0, 2], [3, 5]], [[0, 1], [3, 5]], [[1, 3], [5, 5]],
              [[0, 4], [6, 6]], [[0, 9]]]

    attendu = [[[[0, 0], [3, 5]], [[2, 2], [3, 5]]], [[[3, 5]]],
               [[[1, 1], [5, 5]], [[3, 3], [5, 5]]],
               [[[0, 1], [4, 4], [6, 6]], [[0, 0], [3, 4], [6, 6]]],
               [[[0, 3], [6, 9]]]]

    assert len(entree) == len(attendu)

    for i in range(len(entree)):
        print(f'{entree[i]}')
        assert jouerMilieu(entree[i], 1) in attendu[i]


def test_jouerCote():
    assert jouerCote([[0, 2], [3, 5]], 1, 'D') == [[0, 1], [3, 5]]
    assert jouerCote([[0, 1], [3, 5]], 1, 'D') == [[0, 0], [3, 5]]

    assert jouerCote([[0, 2], [3, 5]], 2, 'D') == [[0, 2], [3, 4]]
    assert jouerCote([[0, 2], [3, 5]], 1, 'G') == [[1, 2], [3, 5]]


def test_partie_ia():
    q = [[0, 5000]]
    while (len(q) > 0):
        q = jouer(ordiJoue(q), q)


@pytest.mark.parametrize('compteur_repet', range(50))
# On lance le test plusieurs fois car les résultats sont aléatoires
def test_quelques_coups(compteur_repet):
    nb = 10
    quilles = [[0, nb - 1]]
    assert quillesVersImprimable(quilles, nb) == "||||||||||"
    jouer("1:M", quilles)
    assert quillesVersImprimable(quilles, nb) == "||||..||||"
    jouer("1:M", quilles)
    assert quillesVersImprimable(quilles, nb) == "|..|..||||"
    jouer("3:D", quilles)
    assert quillesVersImprimable(quilles, nb) == "|..|..|||."
