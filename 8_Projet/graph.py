import turtle
import random
import math
import time

# Variables globales #
etoiles = []
constellations = []
supernovas = []
dernier_temps_update = 0

### Constantes ###
couleurs = ["#058396", "#0275A6", "#827E01", "#fc99da"]
couleur_par_defaut = "white"

limite_x = 960
limite_y = 540

vitesse = 5

vitesse_x = 1 * vitesse
vitesse_y = 0 * vitesse  # Je n'ai pas réussi à faire fonctionner la vitesse y

tick_seconde = 144
##################


def sorti_de_lecran(pos):
    x, y = pos
    return x > limite_x or y > limite_y


def reset_position(pos):
    x, y = pos
    x = -abs(x)
    return x, y


def deplacer(turtle, pos):
    turtle.penup()
    turtle.setpos(pos)
    turtle.pendown()


class Etoile:
    def __init__(self, taille, couleur, pos):
        self.turtle = turtle.Turtle()

        self.turtle.hideturtle()
        self.turtle.color(couleur)
        deplacer(self.turtle, pos)

        self.taille = taille

    def deplacer_de(self, x, y):
        curr_x, curr_y = self.turtle.pos()
        self.deplacer(curr_x + x, curr_y + y)

    def deplacer(self, x, y):
        deplacer(self.turtle, (x, y))

    def dessiner(self):
        self.turtle.clear()
        self.turtle.pendown()
        self.turtle.begin_fill()
        for _ in range(5):
            self.turtle.left(144)
            self.turtle.forward(self.taille)
        self.turtle.end_fill()

        self.__reset_si_besoin()

    def reset(self):
        deplacer(self.turtle, reset_position(self.turtle.pos()))

    def __reset_si_besoin(self):
        if sorti_de_lecran(self.turtle.pos()):
            self.reset()


class Constellation:
    def __init__(self, color=couleur_par_defaut):
        self.etoiles = []
        self.turtle = turtle.Turtle()
        self.turtle.color(color)

    def deplacer_de(self, x, y):
        for etoile in self.etoiles:
            etoile.deplacer_de(x, y)

    def dessiner_lien(self, pos1, pos2):
        # On évite de tracer un trait si les deux étoiles sont trop éloignées
        # Cela se produit généralement quand une étoile a dépassé le bord, mais pas l'autre
        # Ce qui se traduit par un trait immense sur toute la carte
        if abs(pos1[0] - pos2[0]) > 100:
            return

        deplacer(self.turtle, pos1)
        self.turtle.goto(pos2)

    def ajouter(self, etoile):
        self.etoiles.append(etoile)

    def dessiner(self):
        self.turtle.clear()

        # Toutes les étoiles sauf la dernière
        for i in range(len(self.etoiles) - 1):
            self.etoiles[i].dessiner()

            orig = self.etoiles[i].turtle.pos()
            target = self.etoiles[i + 1].turtle.pos()

            self.dessiner_lien(orig, target)

        self.dessiner_lien(self.etoiles[-2].turtle.pos(),
                           self.etoiles[-1].turtle.pos())
        self.etoiles[-1].dessiner()

        self.__reset_si_besoin()

    def __reset_si_besoin(self):
        sur_ecran = False
        for etoile in self.etoiles:
            if not sorti_de_lecran(etoile.turtle.pos()):
                sur_ecran = True
                break

        if not sur_ecran:
            for etoile in self.etoiles:
                etoile.reset()


class Supernova:
    def __init__(self, couleurs, nombre=30, taille=100, ecart=0.15, cotes=8):
        self.turtle = turtle.Turtle()
        self.turtle.hideturtle()

        self.couleurs = couleurs

        self.nombre = nombre
        self.taille = taille
        self.ecart = ecart
        self.cotes = cotes

    def dessiner(self):
        self.turtle.clear()
        taille = self.taille
        orig_pos = self.turtle.pos()
        orig_heading = self.turtle.heading()

        angle = 360 / self.cotes
        for _ in range(self.nombre):
            for _ in range(self.cotes):
                couleur = random.choice(self.couleurs)
                self.turtle.color(couleur)
                self.turtle.forward(taille)
                self.turtle.left(angle)
            taille = self.__preparer_trait_suivant(taille, angle)

        deplacer(self.turtle, orig_pos)
        self.turtle.setheading(orig_heading)

        self.__reset_si_besoin()

    def deplacer_de(self, x, y):
        curr_x, curr_y = self.turtle.pos()
        deplacer(self.turtle, (curr_x + x, curr_y + y))

    def move(self, pos):
        deplacer(self.turtle, pos)

    def reset(self):
        x, y = reset_position(self.turtle.pos())

        # On veut être sûr que la supernova ne soit pas coupée
        x -= self.taille

        self.move((x, y))

    #Voir le schéma du problème 3 sur les énoncés itératifs
    def __preparer_trait_suivant(self, taille, GBK):
        GB = taille * self.ecart
        self.turtle.forward(GB)

        GK = GB * math.sin(math.radians(GBK))
        BK = math.sqrt(GB**2 - GK**2)

        FK = taille - GB + BK
        FG = math.sqrt(FK**2 + GK**2)
        GFB = math.degrees(math.asin(GK / FG))

        self.turtle.left(GFB)

        return FG

    def __reset_si_besoin(self):
        x, y = self.turtle.pos()

        # On veut être sûr que la supernova ait totalement disparu de l'écran, pas seulement son centre
        x = x - self.taille

        if sorti_de_lecran((x, y)):
            self.reset()


#a function for moving the turtle to a random location
def emplacement_aleatoire(posMin=(-limite_x, -limite_y),
                          posMax=(limite_x, limite_y)):
    x = random.randint(posMin[0], posMax[0])
    y = random.randint(posMin[1], posMax[1])
    return x, y


def dessiner_etoiles():
    for etoile in etoiles:
        etoile.dessiner()


def dessiner_constellations():
    for constellation in constellations:
        constellation.dessiner()


def dessiner_supernovas():
    for supernova in supernovas:
        supernova.dessiner()


#a function for dessinering a etoile of a particular size
def init_etoile(etoile_taille, etoile_couleur, pos):
    etoile = Etoile(etoile_taille, etoile_couleur, pos)
    etoiles.append(etoile)


#a function for dessinering a small galaxy of etoiles
def init_galaxie(nombre_etoiles):
    x, y = emplacement_aleatoire()
    #dessiner lots of small coloured etoiles
    for _ in range(nombre_etoiles):
        angle = random.randint(-180, 180)
        dist = random.randint(5, 20)
        x += math.cos(angle) * dist
        y += math.sin(angle) * dist
        #dessiner a small etoile in a random colour
        init_etoile(2, random.choice(couleurs), (x, y))


#a function for dessinering a joined constellation of etoiles
def init_constellation(nombre_etoiles):
    #first dessiner all etoiles except the last one,
    #joined by lines, like this: *--*--*--
    x, y = emplacement_aleatoire()
    prevX, prevY = x, y
    const = Constellation()
    for _ in range(nombre_etoiles):
        first = Etoile(random.randint(7, 15), couleur_par_defaut, (x, y))

        angle = random.randint(-90, 90)
        distance = random.randint(30, 70)
        x = prevX + distance * math.cos(math.radians(angle))
        y = prevY + distance * math.sin(math.radians(angle))

        second = Etoile(random.randint(7, 15), couleur_par_defaut, (x, y))

        const.ajouter(first)
        const.ajouter(second)

        prevX = x
        prevY = y
    constellations.append(const)


def initSupernova(taille):
    nova = Supernova(couleurs, taille=taille)
    supernovas.append(nova)


def init():
    for _ in range(50):
        init_etoile(random.randint(5, 25), couleur_par_defaut,
                    emplacement_aleatoire())

    for _ in range(3):
        init_galaxie(30)

    for _ in range(10):
        init_constellation(random.randint(4, 7))

    initSupernova(random.randint(40, 80))


def avancer_astres():
    for etoile in etoiles:
        etoile.deplacer_de(vitesse_x, vitesse_y)

    for c in constellations:
        c.deplacer_de(vitesse_x, vitesse_y)

    for s in supernovas:
        s.deplacer_de(vitesse_x, vitesse_y)


def dessiner_astres():
    #this will dessiner a dark blue background
    turtle.bgcolor("MidnightBlue")
    turtle.tracer(False)
    dessiner_etoiles()
    dessiner_constellations()
    dessiner_supernovas()


def attendre_frame_suivante():
    global dernier_temps_update
    delta = time.time() - dernier_temps_update
    while delta < 1 / tick_seconde:
        time.sleep(delta / 2)
        delta = time.time() - dernier_temps_update
    dernier_temps_update = time.time()


if __name__ == "__main__":
    turtle.tracer(False)
    init()

    while True:
        avancer_astres()
        dessiner_astres()
        attendre_frame_suivante()
