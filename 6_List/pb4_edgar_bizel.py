"""
On suppose que les résultats en "algorithmique et programmation" sont stockés dans une liste où chaque élément de la liste représente un élève et sa note. On suppose que chaque élève est lui-même représenté par une liste contenant 3 informations : le nom de l'élève, le prénom de l'élève, et la note obtenue. Voici un exemple d'un telle liste:

[["Collavizza","Hélène",6],["Allais","Alphonse",14],["Reed","Lou",12]]

On vous demande d'écrire un programme qui permet d'afficher les élèves et leurs notes, de calculer la moyenne de la classe et d'afficher le major de la promotion. Conseil: Vous devez écrire une fonction ou procédure pour chaque action possible. Exemple d'exécution :

Voulez-vous :
   1. afficher les élèves
   2. calculer la moyenne de la classe
   3. trouver le major de promo
   4. sortir
votre choix : 3

Le major de promo est Alphonse Allais

Voulez-vous :
   1. afficher les élèves
   2. calculer la moyenne de la classe
   3. trouver le major de promo
   4. sortir
votre choix : 2

La moyenne de la classe est 10.666666
"""