"""
Produit matriciel 
Écrire un programme qui permet de lire une matrice m, 
un vecteur v et de calculer le produit de m par v. Aide : décomposer le 
problème en plusieurs fonctions. Il faut lire la matrice et la renvoyer, 
il faut lire le vecteur et le renvoyer, il faut appeler la fonction qui 
calcule le produit et le renvoie. 
"""

from typing import List


def initMatrix(row: int, column: int) -> List[List[int]]:
    res = [[]]
    res.clear()  #Removes default line
    for i in range(row):
        res.append([])
        for _ in range(column):
            res[i].append(0)

    return res


def matrixProduct(matOne: List[List[int]],
                  matTwo: List[List[int]]) -> List[List[int]]:
    assert len(matOne[0]) == len(matTwo)

    res = initMatrix(row = len(matOne), column =len(matTwo[0]))

    #Calculate product
    for j in range(len(matOne)):
        for k in range(len(matTwo)):
            
        
    return res


def testMatrixProduct() -> None:
    matOne = [[1, 1], [2, 3], [1, -1]]
    matTwo = [[0, 1, -1, -2], [-3, -2, 0, 1]]

    res = matrixProduct(matOne, matTwo)
    expected = [[-3, -1, -1, -1], [-9, -4, -2, -1], [3, 3, -1, -3]]

    assert res == expected

    print("Test passés")


if __name__ == '__main__':
    testMatrixProduct()