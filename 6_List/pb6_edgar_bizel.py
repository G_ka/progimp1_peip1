"""
Gestion d'un stock de CD. Vous devez gérer un stock de CD qui sont stockés dans une liste de la forme suivante :
[["Suites violoncelle seul","Bylsma","classique",23.50,1], ["No love","Eminem","rap",25.60,5], ["J'appuie sur la gâchette","NTM","rap",13.50,10], ["Symphonie du nouveau monde","Dvorak","classique",30.70,1], ["Adieu tristesse","Arthur H.", "chanson francaise",15.2,10], ["Mister mystère","M","chanson francaise",25.2,3], ["Je dis M","M","chanson francaise",25.2,3],["Pacific 231","Raphaël","chanson francaise",28.6,8]]

Chaque élément de la liste est un CD représenté par une liste contenant :

    le titre du CD,
    le nom du groupe,
    le type du CD (classique, rap, chanson française),
    le prix,
    le nombre d'exemplaires en stock.


Vous devez fournir un menu qui permet :

    - D'afficher tous les CD
    - D'afficher les CD d'une catégorie donnée (e.g. rap)
    - D'afficher les CD d'un groupe (e.g. M)
    - De commander un CD. Pour cela, vous devez demander le nom du groupe et le titre de 
    l'album, vous assurer que ce CD est en stock puis demander le nombre d'exemplaires 
    souhaités, et les soustraire du stock s'il y en a assez.
    - De quitter le menu.

L'utilisateur doit pouvoir enchaîner ces actions tant qu'il ne souhaite pas sortir du menu. 
Quand il quitte le menu, on affiche un récapitulatif de ses commandes avec le montant total.

Évidemment, vous devez gérer les cas de mauvaise utilisation, par exemple si l'on commande 
un CD de Chantal Goya, ou si l'on commande 3 exemplaires puis 4 exemplaires du CD No love de Eminem.
Pour les plus avancés, vous pouvez aussi gérer un panier. Dans ce cas, les CD qui sont dans le 
panier doivent être momentanément supprimés du stock (pour ne pas commander un CD qui n'existe plus) 
mais doivent être rajoutés au stock si l'utilisateur les retire de son panier ou ne passe pas sa commande.
Aide : les fonctions sont indispensables !!! 
"""

