"""
Écrire une fonction permute qui effectue une permutation circulaire aléatoire des lettres d'un mot, 
c'est à dire qui fait "tourner" le mot vers la gauche à partir d'un indice tiré au hazard. 
Par exemple, pour le mot bonjour, si l'indice tiré est 2 on obtient njourbo et si l'indice tiré 
est 4 on obtient ourbonj. Utiliser cette fonction pour lire une liste de mots et afficher 
la permutation de chacun de ces mots
"""

from typing import List
import random


def rotate(word: str, shiftCount: int) -> List[str]:

    res = []
    for i in range(len(word)):
        idx = (i + shiftCount) % len(word)
        res.append(word[idx])

    return res


def randomRotate(word: str) -> List[str]:
    shiftCount = random.randint(0, len(word))
    return rotate(word, shiftCount)


def testRotate():
    assert rotate("Bonjour", 4) == ['o', 'u', 'r', 'B', 'o', 'n', 'j']
    print("Tests passés")


if __name__ == '__main__':
    testRotate()
