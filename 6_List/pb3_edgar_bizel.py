"""
Triangle de Pascal. On veut afficher le triangle de Pascal des coefficients binomiaux 
comme présenté dans http://www.jybaudot.fr/Maths/binome.html 
Écrire la fonction pascalFonction(l) qui étant donné une liste contenant 
les coefficients (n k) pour tout k=1 à n, renvoie la liste des coefficients 
(n+1 k) pour tout k=1 à n+1. Par exemple, pascalFonction([1,4,6,4,1]) 
renvoie [1, 5, 10, 10, 5, 1]. Utiliser ensuite cette fonction dans la 
procédure pascal(n) qui affiche le triangle de Pascal jusqu'au rang n.
"""
from typing import List


def pascalNextLine(rowN: List[int]) -> List[int]:
    res = [1]
    for i in range(len(rowN)):
        isFirst = i == 0
        isLast = i == len(rowN) - 1
        if isFirst:
            continue  #Always begin with 1

        res.append(rowN[i] + rowN[i - 1])

        if isLast:
            res.append(1)  # Always end with 1

    return res


def printPascal(n: int) -> None:
    print([1])
    line = [1, 1]
    for i in range(n - 1):
        print(line)
        line = pascalNextLine(line)


printPascal(10)
