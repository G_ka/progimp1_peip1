"""
La suite audio-active consiste à lire à voix haute des termes et à compter les chiffres qui constituent le terme. 
La suite démarre avec le terme 1. Il y a "un 1" donc le terme suivant est 11. Il y a "deux 1" donc 
le terme suivant est 21. Puisqu'il y a "un 2 et un 1" le terme suivant est 1211.
Pour cet exercice, on suppose que les termes de la suite sont stockés dans des 
chaînes de caractères. Écrire la fonction audioActive(terme) qui prend un terme de la 
suite et renvoie le suivant. Par exemple, audioActive("1211") doit renvoyer "111221". 
Utiliser cette fonction dans un programme qui demande à l'utilisateur le 
nombre de termes de la suite audio-active qu'il veut calculer et qui les affiche. Exemple d'exécution :

combien de termes (>=2) ? 7
terme 1: 1
terme 2: 11
terme 3 : 21
terme 4 : 1211
terme 5 : 111221
terme 6 : 312211
terme 7 : 13112221
"""

from typing import Tuple


def eatAdjacent(terme: str) -> Tuple[str, int]:
    """ 
    Supprime les premiers caractères identiques adjacents.
    Retourne le nombre de caractères supprimés et la chaine modifiée
    Exemple : eatAdjacent('1132') renverra ('32', 2)
    """

    i = 1
    while i < len(terme) and terme[i] == terme[i - 1]:
        i += 1
    return (terme[i:], i)


def audioActive(terme: str) -> str:
    res = ""
    # On extrait les caractères petit à petit
    while len(terme) >= 2:
        currentFirstChar = terme[0]
        terme, count = eatAdjacent(terme)
        res += str(count) + currentFirstChar

    # On a traité tous les caractères, sauf peut-être le dernier.
    # S'il reste un caractère, cela signifie qu'il n'est présent qu'une seule fois
    if len(terme):
        res += '1' + terme
    return res


def test():
    assert (audioActive("1") == "11")
    assert (audioActive("11") == "21")
    assert (audioActive("21") == "1211")
    assert (audioActive("1211") == "111221")
    assert (audioActive("111221") == "312211")
    assert (audioActive("312211") == "13112221")
    assert (audioActive("132221") == "11133211")
    print("Tests passés")


if __name__ == '__main__':
    test()

    print("Bienvenue dans la suite audio active")
    terme = int(input("Jusqu'à quel terme (nombre entier) ?"))

    res = "1"
    for i in range(1, terme + 1):
        print(f"{i} : {res}")
        res = audioActive(res)
