"""
Renversant (bis). Réécrire la fonction renverse en utilisant un for. 
"""


def renverse(s):
    reversed = ""
    for c in s:
        reversed = c + reversed
    return reversed
