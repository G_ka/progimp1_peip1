"""
On vous demande maintenant d'écrire une fonction de cryptographie basée 
sur l'utilisation d'un alphabet désordonné. Le principe est le suivant : 
une chaîne de caractères sert de clé et permet de créer une table de 
codage. Cette table contient au début les lettres de la clé (une seule fois) 
puis contient, par ordre alphabétique, les lettres de l'alphabet qui ne 
sont pas contenues dans la clé. Par exemple, si la clé est "j'aime les 
cerises" la table est "jaimelscrbdfghknopqtuvwxyz". Si la clé est 
"j'aime les cerises qui piquent le nez" la table est "jaimelscrqupntzbdfghkovwxy". 
Une fois cette table construite, le codage consiste à remplacer le caractère 
de position i dans l'alphabet par le caractère de position i dans la table. 
Donc avec la clé "j'aime les cerises", le a est remplacé par j, le b est 
remplacé par a.
"""

from typing import List

alphabet = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
]


# Performance could probably be improved
def generateAlphabet(key: str) -> List[str]:
    res = []
    for c in key:
        if not c in alphabet:
            continue

        if not c in res:
            res.append(c)

    for c in alphabet:
        if not c in res:
            res.append(c)

    return res


# Coding and decoding is exactly the same, but with the opposite code
def cesarCommonHelper(sentence: str, sourceLetters: List[str],
                      encryptedletters: List[str]):
    res = ""
    for c in sentence:
        if not c in sourceLetters:
            res += c
            continue
        idx = sourceLetters.index(c)
        converted = encryptedletters[idx]
        res += converted
    return res


def codeCesar(sentence: str, encryptedLetters: List[str]) -> str:
    return cesarCommonHelper(sentence, alphabet, encryptedLetters)


def decodeCesar(sentence: str, encryptedLetters: List[str]) -> str:
    return cesarCommonHelper(sentence, encryptedLetters, alphabet)


def testCesar():
    assert len(alphabet) == len(generateAlphabet(""))

    source = "A long string containing lots of characters"
    "wxcvbnqsdfghjklmazertyuiop123456789&é'(-è_çà"

    encryptedLetters = generateAlphabet(
        "j aime les cerises qui piquent le nez")

    coded = codeCesar(source, encryptedLetters)
    decoded = decodeCesar(coded, encryptedLetters)

    assert coded != source and decoded == source

    print("Tests passés")


if __name__ == '__main__':
    testCesar()
