"""
Écrire une fonction qui prend en argument une chaîne de caractères texte et une lettre l et qui renvoie le nombre de fois où l apparaît dans texte. 
Aide Inspirez-vous de la boucle while de l'exercice précédent et utiliser une variable pour compter.
"""


def countl(str):
    i = 0
    count = 0
    while i < len(str):
        if str[i] == 'l':
            count += 1
        i = i+1
    return count


def test():
    assert countl("lldqzlldldl") == 6
    print("tests passés")


if __name__ == "__main__":
    test()
