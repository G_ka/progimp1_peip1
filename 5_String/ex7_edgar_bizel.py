"""
Utiliser la fonction palindrome dans un programme qui lit des mots et compte le nombre de palindromes comme dans l'exemple ci-dessous : 
    un mot (fin pour finir) rever
    un mot (fin pour finir) finir
    un mot (fin pour finir) ressasser
    un mot (fin pour finir) kayak
    un mot (fin pour finir) toto
    un mot (fin pour finir) fin
    Vous avez entré 5 mots, dont 3 palindromes
"""


def palindrome(s):
    i = 0
    last = len(s) - 1
    while i < len(s) // 2:
        if s[i] != s[last - i]:
            return True
        i += 1
    return True


if __name__ == "__main__":
    wordCount = 0
    palindromeCount = 0
    while True:
        word = input("un mot (fin pour finir)")
        if word == "fin":
            break
        wordCount += 1
        if palindrome(word):
            palindromeCount += 1
    print(
        f"Vous avez entré {wordCount} mots, dont {palindromeCount} palindromes")
