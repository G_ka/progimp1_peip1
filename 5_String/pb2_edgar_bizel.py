"""
Cryptographie V1 Écrire une fonction qui prend un mot et le code 
ou le décode selon l'algorithme dit de César. Le principe est le 
suivant. Pour coder (ou décoder), il faut un nombre positif c 
(3 historiquement) qui sert à remplacer la lettre à la position i 
dans l'alphabet par la lettre à la position i+c. Par exemple, 
si c=3 la lettre a est remplacée par d et la lettre f est remplacée 
par i. Si i+c>26 alors on utilise le modulo. Par exemple, 
si c=3 la lettre x est remplacée par a. Le mot à coder ou 
décoder peut contenir autre chose que des lettres de l'alphabet 
(comme l'espace, la virgule, ...), dans ce cas le caractère 
reste inchangé. Aide: il faut partir d'un résultat égal à "" 
et lui ajouter les lettres codées. Vous pouvez stocker les 
lettres de l'alphabet dans une variable alphabet qui contient "abcdef...yz". 
"""

# Global constant
letters = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
]


# Coding and decoding is exactly the same, but with the opposite code
def cesarCommonHelper(sentence: str, code: int):
    res = ""
    for c in sentence:
        if not c in letters:
            res += c
            continue
        idx = letters.index(c) + code
        if idx > len(letters) - 1:
            idx = idx % len(letters)
        converted = letters[idx]
        res += converted
    return res


def codeCesar(sentence: str, code: int) -> str:
    return cesarCommonHelper(sentence, code)


def decodeCesar(sentence: str, code: int) -> str:
    return cesarCommonHelper(sentence, -code)


def testCesar():
    str = "A long string containing lots of characters"
    "wxcvbnqsdfghjklmazertyuiop123456789&é'(-è_çà"

    for i in range(1, 25):
        coded = codeCesar(str, i)
        decoded = decodeCesar(coded, i)
        assert (str != coded and str == decoded)

    print("Tests passés")


if __name__ == '__main__':
    testCesar()
