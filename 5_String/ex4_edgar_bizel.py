"""C'est renversant ! Écrire la fonction "renverse(s)" qui prend un texte s et renvoie s renversé c'est à dire de droite à gauche. 
Par exemple, renverse("bonjour") renvoie "ruojnob". 
Attention, il faut un return et pas seulement des print, on veut pouvoir vérifier que votre fonction est bien faite dans le programme suivant: 
"""


def renverse(s):
    reversed = ""
    for i in range(len(s)):
        reversed = s[i] + reversed
    return reversed
