"""
Exécuter le programme ci-dessus puis modifiez-le pour afficher seulement une lettre sur deux. 
"""

mot = input("un mot ")
i = 0
while i < len(mot):
    if i % 2:
        print(mot[i])
    i = i + 1
