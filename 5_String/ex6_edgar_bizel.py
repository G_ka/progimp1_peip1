"""
Palindrome : un palindrome est un mot qui se lit de la même façon de droite à gauche que de gauche à droite comme "rever" ou "ressasser". 
Écrire une fonction palindrome(s) qui renvoie True si s est un palindrome. Attention au coût de votre fonction donc ne pas construire de choses inutiles. 
Aide : on vous demande d'utiliser une boucle while pour vous arrêter dès qu'on sait qu'il ne s'agit pas d'un palindrome. 
"""


def palindrome(s):
    i = 0
    last = len(s) - 1
    while i < len(s) // 2:
        if s[i] != s[last - i]:
            return False
        i += 1
    return True


def test():
    assert palindrome("kayak")
    assert not palindrome("bonjour")
    assert palindrome("aaabbaaa")
    assert palindrome("ressasser")
    print("Tests passés")


if __name__ == "__main__":
    test()
