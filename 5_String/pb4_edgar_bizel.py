"""
Écrire une procédure majusculise(t) qui affiche t en mettant chaque mot en majuscule. 
Exemples de résultat d'exécution :
majusculise("Un long texte en minuscules")
Un Long Texte En Minuscules
majusculise("Les 14 gendarmes à Saint Tropez")
Les 14 Gendarmes à Saint Tropez

Aide : codes ASCII Le code Unicode est une extension du code ASCII 
dans lequel les codes des lettres minuscules sont compris entre 97 et 122, 
et ceux des lettres majuscules entre 65 et 90. Pour passer 
d'une lettre minuscule à la lettre correspondante en majuscule, 
il suffit donc de soustraire 32 à son code. Pour écrire le caractère 
c en majuscule il faut donc écrire chr(ord(c)-32).
"""


def majusculise(sentence: str) -> str:
    wasSpace = False
    res = ""
    for c in sentence:
        if c == ' ':
            wasSpace = True
            res += c
            continue

        if wasSpace:
            # La fonction upper fonctionne aussi pour les caractères non ASCII,
            # contrairement à chr(ord(c)-32)
            res += c.upper()
        else:
            res += c

        wasSpace = False

    return res


def testMajusculise():
    assert majusculise(
        "Un long texte en minuscules") == "Un Long Texte En Minuscules"
    assert majusculise(
        "Les 14 gendarmes à Saint Tropez") == "Les 14 Gendarmes À Saint Tropez"
    print("Tests passés")


if __name__ == "__main__":
    testMajusculise()
