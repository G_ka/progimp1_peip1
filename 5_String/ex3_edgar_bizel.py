"""
Utiliser la fonction précédente dans un programme qui lit une lettre l, et qui lit des phrases tant que l'utilisateur le souhaite. 
Le programme affiche ensuite le nombre d'apparitions de la lettre l. 
"""


def countl(str):
    i = 0
    count = 0
    while i < len(str):
        if str[i] == 'l':
            count += 1
        i = i+1
    return count


if __name__ == '__main__':
    total = 0
    while True:
        sentence = input("Entrez votre phrase : ")
        total += countl(sentence)

        if input("Entrez 'o' pour continuer : ") != 'o':
            break

    print(f"'l' est apparu {total} fois")
