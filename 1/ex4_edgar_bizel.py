from typing import Final


def validate_input(size: int):
    if (size < 3):
        print("Erreur. La size doit être supérieure ou égale à 3.")
        exit()


def print_square(content: str, size: int):
    whitespace: Final = len(initials) * " "

    filled_line: Final = "\n" + size * initials
    unfilled_line: Final = initials + (size - 2) * whitespace + initials

    print(filled_line + ("\n" + unfilled_line) * (size - 2) + filled_line)


def joke_about_initials(text: str):
    if not text.isalpha():
        print("Seriez-vous un robot ?")
    elif len(text) > 2:
        print("Votre nom doit être très long !")


if __name__ == "__main__":
    initials: Final = input("Initiales ?")
    size: Final = int(input("Taille ?"))

    validate_input(size)
    joke_about_initials(initials)
    print_square(initials, size)