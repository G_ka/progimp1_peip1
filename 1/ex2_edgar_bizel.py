from typing import Final


def eucl_divide(a: int, b: int) -> None:
    if (b > a):
        raise ValueError("Le dénominateur est plus grand que le numérateur")

    quotient = a // b
    reste = a % b
    print(
        f"Division euclidienne de {a} par {b}:\n {a} = {b} * {quotient} + {reste}\n"
    )


if __name__ == "__main__":
    num = int(input("Veuillez entrer le numérateur\n"))
    div = int(input("Veuillez entrer le dénominateur\n"))

    try:
        eucl_divide(num, div)
    except ValueError as err:
        print(f"Une erreur s'est produite: {err}")