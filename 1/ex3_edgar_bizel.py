import math
from typing import Final

# Dans un poulailler, 5 poules ont pondu 14 oeufs en 4 jours. Combien faudrait-il de poules pour récolter x oeufs en y semaines ?
# => En un jour, avec une poule : 14 / (5 * 4) = 14 / 20
ratio: Final = 14 / 20

wished_eggs = float(input("Combien d'oeufs ?"))
wished_days = float(input("En combien de jours ?"))
chicken_required = math.ceil(wished_eggs / wished_days / ratio)
print(f"Il vous faut {chicken_required} poule(s)\n")