"""
Pic et Pouc font un voyage. Ils ont parcouru 184.8 kilomètres en 95 minutes. Il leur reste 157 km à parcourir. 
Quelle a été leur vitesse moyenne jusqu'à présent ? 
Combien de minutes leur faudra-t-il encore avant d'arriver ? 
Aide : vous pouvez utiliser les variables tempsParcouru, distanceParcourue, vitesseMoyenne et distanceRestante, 
et lire les valeurs d'entrée avec la fonction input . 
Voici un exemple d'exécution de votre programme (que vous appellerez vitesse.py) :

Distance parcourue (en km) : 184.8
Temps mis (en mn) : 95 
Vous avez parcouru 184.8 km en 95 mn
Vous avez roulé à la vitesse moyenne de 116.71578947368423 km/h
En roulant à cette vitesse, pour faire les 157 km restants, il vous faudra 80.70887445887445 minutes

Tester votre programme en changeant les valeurs des variables. N'oubliez pas les commentaires !

Pour les plus avancés (et eux seulement) Au moment où ils se sont arrêtés, 
Pic et Pouc on regardé leur montre, il est 13h15. A quelle heure arriveront-ils à destination ? 
(Aide: utilisez 2 variables, une pour l'heure, l'autre pour les minutes et convertissez 13h15 en minutes.)
"""

tempsParcouru = 95  #minutes
distanceParcourue = 184.800  #km
vitesseMoyenne = distanceParcourue / 95
distanceRestante = 157.0
tempsRestant = distanceRestante / vitesseMoyenne

print(f"Distance parcourue (en km) : {distanceParcourue}")
print(f"Temps mis (en mn) : {tempsParcouru}")
print(f"Vous avez parcouru {distanceParcourue} km en {tempsParcouru} mn")
print(f"Vous avez roulé à la vitesse moyenne de {vitesseMoyenne} km/h")
print(
    f"En roulant à cette vitesse, pour faire les {distanceRestante} km restants, il vous faudra {tempsRestant} minutes"
)


def minutesToString(minutes_raw: float) -> str:
    heures = int(minutes_raw // 60)
    minutes = round(minutes_raw % 60)
    return f"{int(heures)}h{minutes}"


#Il est 13h15
tempsActuel = 13 * 60 + 15
tempsArrivee = tempsActuel + tempsRestant

print(f"Vous arriverez à {minutesToString(tempsArrivee)}")